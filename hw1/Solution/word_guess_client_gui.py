from tkinter import *
import datetime
import logging

class WordGuessClient(Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.pack()
        self.create_widgets()
        self.__logger = logging.getLogger('word_guess_client_app.word_guess_client_gui.WordGuessClient')

    def setController(self, controller):
        self.__controller = controller

    def create_widgets(self):
        self.winfo_toplevel().title('Word Guess Client')

        self.__setServerAddressButton = Button(self, text='Set Server Address')
        self.__setServerAddressButton['command'] = self.__onSetServerAddressButtonClick
        self.__setServerAddressButton.grid(row=0, column=1)

        Label(self, text='IP').grid(row=1, column=0)
        self.__ipaddressEntry = Entry(self)
        self.__ipaddressEntry.grid(row=1, column=1)

        Label(self, text='Port #').grid(row=2, column=0)
        self.__portNumberEntry = Entry(self)
        self.__portNumberEntry.grid(row=2, column=1)

        Label(self, text='A#').grid(row=0, column=4)
        self.__aNumEntry = Entry(self)
        self.__aNumEntry.grid(row=0, column=5)

        Label(self, text='Last Name').grid(row=1, column=4)
        self.__lnameEntry = Entry(self)
        self.__lnameEntry.grid(row=1, column=5)

        Label(self, text='First Name').grid(row=2, column=4)
        self.__fnameEntry = Entry(self)
        self.__fnameEntry.grid(row=2, column=5)

        Label(self, text='Alias').grid(row=3, column=4)
        self.__aliasEntry = Entry(self)
        self.__aliasEntry.grid(row=3, column=5)

        self.__tempLabel = Label(self, text="Click New Game to start!")
        self.__tempLabel.grid(row=4, column=3)

        Label(self, text='Guess').grid(row=5, column=2)
        self.__guessText = StringVar()
        self.__guessEntry = Entry(self, textvariable=self.__guessText)
        self.__guessEntry.grid(row=5, column=3)

        self.__makeGuessButton = Button(self)
        self.__makeGuessButton['text'] = 'Send Guess'
        self.__makeGuessButton['command'] = self.__onMakeGuessButtonClick
        self.__makeGuessButton.grid(row=5, column=4)

        self.__heartBeatLabel = Label(self, text="")
        self.__heartBeatLabel.grid(row=6, column=3)

        self.__errorLabel = Label(self, text='')
        self.__errorLabel.grid(row=7, column=3)

        self.__newGameButton = Button(self)
        self.__newGameButton['text'] = 'New Game'
        self.__newGameButton['command'] = self.__onNewGameButtonClick
        self.__newGameButton.grid(row=8, column=3)

        self.__getHintButton = Button(self)
        self.__getHintButton['text'] = 'Get Hint'
        self.__getHintButton['command'] = self.__onGetHintButtonClick
        self.__getHintButton.grid(row=9, column=3)

        self.__exitButton = Button(self, text='Exit')
        self.__exitButton['command'] = self.__onExitButtonClick
        self.__exitButton.grid(row=10, column=3)

    def update(self, gameDefinition):
        self.__tempLabel['text'] = 'GameId: {0}\nHint: {1}\nDefinition: {2}'.format(gameDefinition['gameId'], gameDefinition['hint'], gameDefinition['definition'])
        self.__guessText.set(gameDefinition['hint'])

    def displayCorrect(self, score):
        self.__tempLabel['text'] = 'You win! Score = {0}'.format(score)

    def displayHeartBeat(self):
        self.__heartBeatLabel['text'] = 'HeartBeat: ' + str(datetime.datetime.now().time())

    def displayError(self, errorMsg):
        self.__errorLabel['text'] = errorMsg

    def __onSetServerAddressButtonClick(self):
        self.__controller.updateServerAddress((self.__ipaddressEntry.get(), 
            int(self.__portNumberEntry.get())))

    def __onMakeGuessButtonClick(self):
        self.__controller.sendGuessMsg(self.__guessText.get())

    def __onNewGameButtonClick(self):
        aNum = self.__aNumEntry.get()
        lname = self.__lnameEntry.get()
        fname = self.__fnameEntry.get()
        alias = self.__aliasEntry.get()
        self.__controller.sendStartNewGameMsg(aNum, lname, fname, alias)

    def __onGetHintButtonClick(self):
        self.__controller.sendGetHintMsg()

    def __onExitButtonClick(self):
        self.__controller.sendExitMsg()
        self.quit()


