from msg_controller import encodeMsg, decodeMsg, getMsgCode, NEW_GAME_MSG_CODE, GAME_DEF_MSG_CODE, GUESS_MSG_CODE, ANSWER_MSG_CODE, GET_HINT_MSG_CODE, HINT_MSG_CODE, EXIT_MSG_CODE, ACK_MSG_CODE, ERROR_MSG_CODE, HEARTBEAT_MSG_CODE
from transmitter import UDPTransmitter
import logging

class WordGuessController:
  def __init__(self, gui):
    self.__currentGameDefinition = None
    self.__currentAddress = None
    self.__transmitter = UDPTransmitter(('',0), self.__handleMsgRecv)
    self.__gui = gui
    self.__logger = logging.getLogger(
      'word_guess_client_app.game_controller.WordGuessController')

  def getCurrentGameDefinition(self):
    return self.__currentGameDefinition

  def updateServerAddress(self, address):
    self.__currentAddress = address

  def sendStartNewGameMsg(self, aNum, lname, fname, alias):
    self.__logger.info('Starting new game: {}, {}, {}, {} at address {}'.format(aNum, lname, fname, alias, self.__currentAddress))
    self.__transmitter.sendto(encodeMsg(NEW_GAME_MSG_CODE, aNum, lname, fname, alias), self.__currentAddress)

  def sendGuessMsg(self, guess):
    self.__logger.info(
      'Sending guess: {} to address {}'.format(guess, self.__currentAddress))
    self.__transmitter.sendto(encodeMsg(GUESS_MSG_CODE, self.__currentGameDefinition['gameId'], guess), self.__currentAddress)

  def sendGetHintMsg(self):
    self.__logger.info(
      'Sending get hint msg to address {}'.format(self.__currentAddress))
    self.__transmitter.sendto(encodeMsg(GET_HINT_MSG_CODE, self.__currentGameDefinition['gameId']), self.__currentAddress)

  def sendExitMsg(self):
    self.__logger.info(
      'Sending exit msg to address {}'.format(self.__currentAddress))
    self.__transmitter.sendto(encodeMsg(EXIT_MSG_CODE, self.__currentGameDefinition['gameId']), self.__currentAddress)

  def sendAckMsg(self):
    self.__logger.info(
      'Sending ack msg to address {}'.format(self.__currentAddress))
    self.__transmitter.sendto(encodeMsg(ACK_MSG_CODE, self.__currentGameDefinition['gameId']), self.__currentAddress)

  def updateWithGameDefinition(self, gameDefMsg):
    self.__currentGameDefinition = gameDefMsg
    self.updateGUI(self.__currentGameDefinition)

  def updateWithAnswer(self, answerMsg):
    if not self.isCurrentGameId(answerMsg['gameId']):
      return
    if answerMsg['result'] == True:
      self.winGame(answerMsg['score'])
      return
    self.__currentGameDefinition['hint'] = answerMsg['hint']
    self.updateGUI(self.__currentGameDefinition)

  def updateWithHint(self, hintMsg):
    if not self.isCurrentGameId(hintMsg['gameId']):
      return
    self.__currentGameDefinition['hint'] = hintMsg['hint']
    self.updateGUI(self.__currentGameDefinition)

  def handleError(self, errorMsg):
    self.__gui.displayError(errorMsg['errorText'])

  def respondToHeartbeat(self, heartbeatMsg):
    if not self.isCurrentGameId(heartbeatMsg['gameId']):
      return
    self.sendAckMsg()
    self.__gui.displayHeartBeat()

  def updateGUI(self, gameDefinition):
    self.__gui.update(gameDefinition)

  def winGame(self, score):
    self.__gui.displayCorrect(score)

  def isCurrentGameId(self, gameId):
    return gameId == self.__currentGameDefinition['gameId']

  def __handleMsgRecv(self, msgBytes):
    msgCode = getMsgCode(msgBytes)
    decodedMsg = decodeMsg(msgBytes)
    if msgCode == GAME_DEF_MSG_CODE:
      self.updateWithGameDefinition(decodedMsg)
    elif msgCode == ANSWER_MSG_CODE:
      self.updateWithAnswer(decodedMsg)
    elif msgCode == HINT_MSG_CODE:
      self.updateWithHint(decodedMsg)
    elif msgCode == ACK_MSG_CODE:
      pass
    elif msgCode == ERROR_MSG_CODE:
      self.handleError(decodedMsg)
    elif msgCode == HEARTBEAT_MSG_CODE:
      self.respondToHeartbeat(decodedMsg)
    else:
      self.__logger.error('Received unknown msg type: {}, msg: {}'.format(msgCode, msgBytes))







