import struct
import logging

msg_ctrl_logger = logging.getLogger('word_guess_client_app.msg_controller')

NEW_GAME_MSG_CODE = 1
GAME_DEF_MSG_CODE = 2
GUESS_MSG_CODE = 3
ANSWER_MSG_CODE = 4
GET_HINT_MSG_CODE = 5
HINT_MSG_CODE = 6
EXIT_MSG_CODE = 7
ACK_MSG_CODE = 8
ERROR_MSG_CODE = 9
HEARTBEAT_MSG_CODE = 10

def encodeString(s):
  b = bytes(s, 'utf-16-be')
  return len(b).to_bytes(2, 'big') + b

def encodeNewGameMsg(aNum, lname, fname, alias):
  return bytes([0,NEW_GAME_MSG_CODE]) + encodeString(aNum) + encodeString(lname) + encodeString(fname) + encodeString(alias)

def encodeGuessMsg(gameId, guess):
  return bytes([0,GUESS_MSG_CODE]) + gameId.to_bytes(2, 'big') + encodeString(guess)

def encodeGetHintMsg(gameId):
  return bytes([0,GET_HINT_MSG_CODE]) + gameId.to_bytes(2, 'big')

def encodeExitMsg(gameId):
  return bytes([0,EXIT_MSG_CODE]) + gameId.to_bytes(2, 'big')

def encodeAckMsg(gameId):
  return bytes([0,ACK_MSG_CODE]) + gameId.to_bytes(2, 'big')

def getMsgEncoder(msgCode):
  if msgCode == NEW_GAME_MSG_CODE:
    return encodeNewGameMsg
  elif msgCode == GUESS_MSG_CODE:
    return encodeGuessMsg
  elif msgCode == GET_HINT_MSG_CODE:
    return encodeGetHintMsg
  elif msgCode == EXIT_MSG_CODE:
    return encodeExitMsg
  elif msgCode == ACK_MSG_CODE:
    return encodeAckMsg
  msg_ctrl_logger.error('Unable to encode message of type: {}'.format(msgCode))
  return None

def encodeMsg(msgCode, *args):
  return getMsgEncoder(msgCode)(*args)

def decodeGameDefMsg(msgBytes):
  currentPos = 2
  gameId = struct.unpack('>H', msgBytes[currentPos:currentPos+2])[0]
  currentPos += 2
  len_hint = struct.unpack('>H', msgBytes[currentPos:currentPos+2])[0]
  currentPos += 2
  hint = msgBytes[currentPos:currentPos+len_hint].decode('utf-16-be')
  currentPos += len_hint
  len_def = struct.unpack('>H', msgBytes[currentPos:currentPos+2])[0]
  currentPos += 2
  definition = msgBytes[currentPos:currentPos+len_def].decode('utf-16-be')
  return {'gameId':gameId, 'hint':hint, 'definition':definition}

def decodeAnswerMsg(msgBytes):
  currentPos = 2
  gameId = struct.unpack('>H', msgBytes[currentPos:currentPos+2])[0]
  currentPos += 2
  if msgBytes[currentPos:currentPos+1] == b'\x00':
    result = False
  else:
    result = True
  currentPos += 1
  score = struct.unpack('>H', msgBytes[currentPos:currentPos+2])[0]
  currentPos += 2
  len_hint = struct.unpack('>H', msgBytes[currentPos:currentPos+2])[0]
  currentPos += 2
  hint = msgBytes[currentPos:currentPos+len_hint].decode('utf-16-be')
  return {'gameId':gameId, 'result':result, 'score':score, 'hint':hint}

def decodeHintMsg(msgBytes):
  currentPos = 2
  gameId = struct.unpack('>H', msgBytes[currentPos:currentPos+2])[0]
  currentPos += 2
  len_hint = struct.unpack('>H', msgBytes[currentPos:currentPos+2])[0]
  currentPos += 2
  hint = msgBytes[currentPos:currentPos+len_hint].decode('utf-16-be')
  return {'gameId':gameId, 'hint':hint}

def decodeAckMsg(msgBytes):
  currentPos = 2
  return {'gameId':struct.unpack('>H', msgBytes[currentPos:currentPos+2])[0]}

def decodeErrorMsg(msgBytes):
  currentPos = 2
  gameId = struct.unpack('>H', msgBytes[currentPos:currentPos+2])[0]
  currentPos += 2
  len_err_txt = struct.unpack('>H', msgBytes[currentPos:currentPos+2])[0]
  currentPos += 2
  errorText = msgBytes[currentPos:currentPos+len_err_txt].decode('utf-16-be')
  return {'gameId':gameId, 'errorText':errorText}

def decodeHeartBeatMsg(msgBytes):
  currentPos = 2
  return {'gameId':struct.unpack('>H', msgBytes[currentPos:currentPos+2])[0]}

def getMsgCode(msgBytes):
  return struct.unpack('>H', msgBytes[0:2])[0]

def getMsgDecoder(msgCode):
  if msgCode == GAME_DEF_MSG_CODE:
    return decodeGameDefMsg
  if msgCode == ANSWER_MSG_CODE:
    return decodeAnswerMsg
  if msgCode == HINT_MSG_CODE:
    return decodeHintMsg
  if msgCode == ACK_MSG_CODE:
    return decodeAckMsg
  if msgCode == ERROR_MSG_CODE:
    return decodeErrorMsg
  if msgCode == HEARTBEAT_MSG_CODE:
    return decodeHeartBeatMsg
  msg_ctrl_logger.error('Unable to decode message of type: {}'.format(msgCode))

def decodeMsg(msgBytes):
  return getMsgDecoder(getMsgCode(msgBytes))(msgBytes)


