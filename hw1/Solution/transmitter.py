from event_system import Event, EventSystem
import threading
import socket
import select
import logging

eventSystem = EventSystem()

class UDPTransmitter:
  def __init__(self, socketBindParams, eventHandler):
    self.__socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    self.__socket.setblocking(False)
    self.__socket.bind(socketBindParams)
    self.__listen = True
    self.__listenThead = None
    eventSystem.addEventHandler('msg_recv', eventHandler)
    self.__listenThead = threading.Thread(target=self.__listenThreadRoutine, daemon=True)
    self.__listenThead.start()
    self.__logger = logging.getLogger('word_guess_client_app.transmitter.UDPTransmitter')

  def __listenThreadRoutine(self):
    while self.__listen:
      try:
        recvBytes = self.__socket.recv(2048)
      except:
        continue
      self.__logger.debug('Received msg: {}'.format(recvBytes))
      eventSystem.postEvent(Event('msg_recv', msgBytes=recvBytes))

  def sendto(self, sendBytes, address):
    sent = False
    maxAttempts = 1000
    i = 0
    while not sent and i <= maxAttempts:
      try:
        self.__socket.sendto(sendBytes, address)
      except:
        i += 1
        continue
      sent = True
    if sent:
      self.__logger.debug('Sent msg: {}'.format(sendBytes))
    else:
      self.__logger.warning('Unable to send message: {0}\n Tried {1} times'.format(sendBytes, maxAttempts))

  def __del__(self):
    self.__listen = False