from collections import deque
import threading
import logging

event_system_logger = logging.getLogger('word_guess_client_app.event_system')

class Event:
  def __init__(self, type, **data):
    self.type = type
    self.data = data

class EventSystem:
  def __init__(self, numWorkerThreads = 4):
    self.__eventQueue = deque()
    self.__eventTypeToHandler = {}
    self.__continue = True
    self.__threadPool = []
    for i in range(numWorkerThreads):
      t = threading.Thread(target=self.__eventListenerRoutine, 
        daemon=True)
      t.start()
      self.__threadPool.append(t)

  def __eventListenerRoutine(self):
    while self.__continue:
      if len(self.__eventQueue) != 0:
        try:
          e = self.__eventQueue.popleft()
        except IndexError:
          event_system_logger.warning(
            'The event queue used is not thread safe as multiple threads tried to pop last element')
          continue
        for h in self.__eventTypeToHandler[e.type]:
          h(**e.data)

  def addEventHandler(self, eventType, eventHandler):
    if eventType in self.__eventTypeToHandler:
      event_system_logger.debug(
        'Adding new event handler to existing event: {}'.format(eventType))
      self.__eventTypeToHandler[eventType].append(eventHandler)
    else:
      event_system_logger.debug(
        'Adding new event handler to new event: {}'.format(eventType))
      self.__eventTypeToHandler[eventType] = [eventHandler]

  def postEvent(self, event):
    event_system_logger.debug('Event posted: {}'.format(event))
    self.__eventQueue.append(event)

  def __del__(self):
    self.__continue = False