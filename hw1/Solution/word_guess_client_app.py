from word_guess_client_gui import WordGuessClient
import tkinter as tk
from game_controller import WordGuessController
import logging
import configparser

def main():
  config = configparser.ConfigParser()
  config.read('config.ini')
  defaultConfig = config['DEFAULT']
  defaultLogFile = defaultConfig['log_file']
  defaultLogLevel = defaultConfig['logging_level'].upper()
  logger = logging.getLogger('word_guess_client_app')
  logger.setLevel(defaultLogLevel)
  logFileHandler = logging.FileHandler(defaultLogFile)
  logFileHandler.setLevel(defaultLogLevel)
  formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
  logFileHandler.setFormatter(formatter)
  logger.addHandler(logFileHandler)
  logger.info('Creating root UI element')
  root = tk.Tk()
  logger.info('Creating WordGuessClient application')
  app = WordGuessClient(master=root)
  logger.info('Creating WordGuessController')
  ctrl = WordGuessController(app)
  logger.info('Setting controller in application')
  app.setController(ctrl)
  logger.info('Beginning main event loop')
  app.mainloop()

if __name__ == '__main__':
  main()