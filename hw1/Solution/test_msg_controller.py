import unittest
from msg_controller import *

class TestMsgController(unittest.TestCase):
  
  def test_encodeString(self):
    self.assertEqual(encodeString('Hello'), b'\x00\n\x00H\x00e\x00l\x00l\x00o')

  def test_encodeNewGameMsg(self):
    self.assertEqual(encodeNewGameMsg('A01646465', 'Smith', 'Joshua', 'cpppro'), b'\x00\x01\x00\x12\x00A\x000\x001\x006\x004\x006\x004\x006\x005\x00\n\x00S\x00m\x00i\x00t\x00h\x00\x0c\x00J\x00o\x00s\x00h\x00u\x00a\x00\x0c\x00c\x00p\x00p\x00p\x00r\x00o')

  def test_encodeGuessMsg(self):
    self.assertEqual(encodeGuessMsg(1,'Hello'), b'\x00\x03\x00\x01\x00\n\x00H\x00e\x00l\x00l\x00o')

  def test_encodeGetHintMsg(self):
    self.assertEqual(encodeGetHintMsg(1), b'\x00\x05\x00\x01')

  def test_encodeExitMsg(self):
    self.assertEqual(encodeExitMsg(1), b'\x00\x07\x00\x01')

  def test_encodeAckMsg(self):
    self.assertEqual(encodeAckMsg(1), b'\x00\x08\x00\x01')

  def test_decodeGameDefMsg(self):
    self.assertEqual(decodeMsg(b'\x00\x02\x00\x01\x00\x08\x00_\x00_\x00_\x00_\x00\x0e\x004\x00b\x00l\x00a\x00n\x00k\x00s'), {'gameId':1, 'hint':'____', 'definition':'4blanks'})

  def test_decodeAnswerMsg(self):
    self.assertEqual(decodeMsg(b'\x00\x04\x00\x01\x01\x00\x0c\x00\x08\x00_\x00_\x00_\x00_'), {'gameId':1, 'result':True, 'score':12, 'hint':'____'})

  def test_decodeHintMsg(self):
    self.assertEqual(decodeMsg(b'\x00\x06\x00\x01\x00\x08\x00_\x00_\x00_\x00_'), {'gameId':1, 'hint':'____'})

  def test_decodeAckMsg(self):
    self.assertEqual(decodeMsg(b'\x00\x08\x00\x01'), {'gameId':1})

  def test_decodeErrorMsg(self):
    self.assertEqual(decodeMsg(b'\x00\x09\x00\x01\x00\x12\x00t\x00e\x00s\x00t\x00e\x00r\x00r\x00o\x00r'), {'gameId':1, 'errorText':'testerror'})

  def test_decodeHeartBeatMsg(self):
    self.assertEqual(decodeMsg(b'\x00\n\x00\x01'), {'gameId':1})

if __name__ == '__main__':
  unittest.main()